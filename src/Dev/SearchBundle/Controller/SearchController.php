<?php

namespace Dev\SearchBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    public function indexAction(Request $request, $phrase = NULL)
    {
        if ($request->getMethod() == "POST") {
            $requestData = $request->request->all();
            $phrase = null;
            if (isset($requestData['search']['phrase'])) {
                $phrase = $requestData['search']['phrase'];
            }
            $phrase = str_replace(" ", "+", $phrase);
            return $this->redirect($this->generateUrl('dev_search', array('phrase' => $phrase)));
        }

        $searchResult = null;
        if ($phrase != null) {
            $searchd = $this->get('iakumai.sphinxsearch.search');
            $searchd->setMatchMode(SPH_MATCH_EXTENDED2);
            $searchd->setRankingMode(SPH_RANK_SPH04);

            $searchResult = $searchd->search($phrase, array('test1'));
        }

        $matches = array();
        if (isset($searchResult['matches'])) {
            $matches = $searchResult['matches'];
        }
        return $this->render('DevSearchBundle:Search:index.html.twig', array('matches' => $matches));
    }
}
