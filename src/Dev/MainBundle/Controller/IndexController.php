<?php

namespace Dev\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Route("/page/{pageNumber}", name="paged_index", requirements={"pageNumber" = "\d+"}, defaults={"pageNumber" = 0})
     * @Template()
     */
    public function indexAction(Request $request, $pageNumber = 0)
    {
    	$em = $this->getDoctrine()->getManager();
    	$limit = 20;

    	$posts = $em->createQuery("SELECT p FROM DevBlogBundle:Post p WHERE p.visible = 1 ORDER BY p.addedDate DESC")
    		->setFirstResult($pageNumber * $limit)
    		->setMaxResults($limit)
    		->getResult();

        $paginatorService = $this->get('dev.paginator.service');
        $pages = $paginatorService->getPagesNumber($posts, $limit);

        /* RENDEROWANIE SZABLONU Z BLOGA !!! */
    	return $this->render('DevBlogBundle:Index:index.html.twig', array(
            'posts' => $posts,
            'pages' => $pages
        ));
        //return array('posts' => $posts);
    }
}
