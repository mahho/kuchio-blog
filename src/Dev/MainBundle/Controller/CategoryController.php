<?php

namespace Dev\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Dev\MainBundle\Entity\Category;
use Dev\MainBundle\Form\CategoryType;

/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{

    /**
     * Lists all Category entities.
     *
     * @Route("/", name="category")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DevMainBundle:Category')->findAll();

        return array(
            'entities' => $entities,
        );
    }
 
    /**
     * Finds and displays a Category entity.
     *
     * @Route("/{slug}", name="category_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DevMainBundle:Category')->findOneBySlug($slug);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Category entity.');
        }
        $posts = $em->getRepository('DevBlogBundle:Post')->findByCategory($entity);

        // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'posts' => $posts
            // 'delete_form' => $deleteForm->createView(),
        );
    }

}
