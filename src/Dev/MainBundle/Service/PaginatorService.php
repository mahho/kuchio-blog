<?php
namespace Dev\MainBundle\Service;

use Doctrine\ORM\EntityManger;

class PaginatorService 
{
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }   

    public function getPagesNumber($data, $entriesPerPage) 
    {
        $entries = count($data);
        $pages = $entries / $entriesPerPage;
        return (int)$pages;
    }
}
