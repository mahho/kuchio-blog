<?php
namespace Dev\AdminBundle\Security\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Doctrine\ORM\EntityManager;
use Dev\AdminBundle\Entity\User;
use Dev\AdminBundle\Security\User\WebserviceUser;

class WebserviceUserProvider implements UserProviderInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function loadUserByUsername($username)
    {
        // make a call to your webservice here
        $userData = $this->em->getRepository("DevAdminBundle:User")->findOneByUsername($username);
        // pretend it returns an array on success, false if there is no user

        if ($userData instanceof User) {
            $username = $userData->getUsername();
            $password = $userData->getPassword();
            $salt = $userData->getSalt();
            $roles[] = $userData->getRoles();
            // $token = new UsernamePasswordToken($username, $password, "hfljwhkdhgqe2134kj2h3rkhgkrh1khk1h", $roles);
            // $this->get("security.context")->setToken($token);
            return new WebserviceUser($username, $password, $salt, $roles);
        }

        throw new UsernameNotFoundException(
            sprintf('Username "%s" does not exist.', $username)
        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Dev\AdminBundle\Security\User\WebserviceUser';
    }
}
