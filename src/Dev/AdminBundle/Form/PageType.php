<?php

namespace Dev\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('visible','checkbox', array('attr' => array('checked' => 'checked'), 'required' => false))
            ->add('category', 'entity', array('class' => 'Dev\MainBundle\Entity\Category', 'property' => 'categoryName', 'empty_value' => 'Wybierz', 'required' => false))
            ->add('addedToMenu', 'checkbox', array('attr' => array('checked' => 'checked'), 'required' => false))
            ->add('addedDate', 'datetime', array('data' => new \DateTime(), 'with_seconds' => true, 'date_widget' => "single_text", 'time_widget' => "single_text"))
            ->add('displayDate', 'checkbox', array('attr' => array('checked' => 'checked'), 'required' => false))
            ->add('title')
            ->add('slug')
            ->add('displayTitle', 'checkbox', array('attr' => array('checked' => 'checked'), 'required' => false))
            ->add('content', 'ckeditor')
            ->add('displayContent', 'checkbox', array('attr' => array('checked' => 'checked'), 'required' => false))
            ->add('displayPictures', 'checkbox', array('attr' => array('checked' => 'checked'), 'required' => false))
            ->add('displayComments', 'checkbox', array('attr' => array('checked' => 'checked'), 'required' => false))
            ->add('commentsEnabled', 'checkbox', array('attr' => array('checked' => false), 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Dev\CmsBundle\Entity\Page',
            'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dev_adminbundle_page';
    }
}
