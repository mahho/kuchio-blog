<?php
namespace Dev\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Dev\AdminBundle\Entity\User;
use Dev\AdminBundle\Security\User\WebserviceUser;

class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('blog:create:user')
            ->setDescription('Creating new user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dialog = $this->getHelper('dialog');
        $username = $dialog->ask($output, 'Please enter login: ', false);
        $adminRights = (bool)$dialog->ask($output, 'Admin rights? ', "0");

        $em = $this->getContainer()->get('doctrine')->getManager();
        $user = $em->getRepository('DevAdminBundle:User')->findOneByUsername($username);
        if ($user instanceof User) {
            throw new Exception("Taki użytkownik już istnieje");
        }
        $password = substr(md5(uniqid()), 0, 8);
        $salt = substr(md5(uniqid()), 8, 16);
        $encoderFactory = $this->getContainer()->get('security.encoder_factory');
        // $encoderFactory = new EncoderFactory();
        $user = new User();
        $user->setUsername($username);
        $user->setSalt($salt);
        $roles = array();
        if ($adminRights) {
            $roles[] = "ROLE_ADMIN";
        }
        $user->setRoles($roles);
        
        $webserviceUser = new WebserviceUser($user, $password, $salt, $roles);
        $encoder = $encoderFactory->getEncoder($webserviceUser);
        if ($encoder->isPasswordValid($encoder->encodePassword($password, $salt), $password, $salt)) {
            $user->setPassword($encoder->encodePassword($password, $salt));
            $em->persist($user);
            $em->flush();
            $output->writeln('Password: ' . $password);
        } else {
            throw new Exception("Encoding password error", 1);
        }

    }
}