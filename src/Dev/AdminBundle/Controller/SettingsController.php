<?php

namespace Dev\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Dev\MainBundle\Entity\Settings;
use Dev\AdminBundle\Form\SettingsType;
use Dev\AdminBundle\Form\UserType;

/**
 * Settings controller.
 *
 * @Route("/admin/settings")
 */
class SettingsController extends Controller
{

    /**
     * Lists all Settings entities.
     *
     * @Route("/", name="admin_settings")
     * @Method("GET|POST")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $webserviceUser = $this->get('security.context')->getToken()->getUser();
        $form = $this->createForm(new UserType(), $webserviceUser);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $user = $em->getRepository('DevAdminBundle:User')->findOneByUsername($webserviceUser->getUsername());
            if (!$user) {
                throw new Exception("Uzytkownik nie istnieje", 1);
            }
            $encoderFactory = $this->get('security.encoder_factory');
            $encoder = $encoderFactory->getEncoder($webserviceUser);

            $salt = substr(md5(uniqid()), 8, 16);
            $newpassword = $form->getData()->getPassword();
            $user->setSalt($salt);
            $user->setPassword($encoder->encodePassword($newpassword, $salt));
            $em->persist($user);
            $em->flush();
            $this->get('session')->getFlashBag()->add('notice', 'Hasło zostało zmienione');
            return $this->redirect($this->generateUrl('admin_settings'));
        }
        return array(
            'passwordForm' => $form->createView(),
        );
    }
}
