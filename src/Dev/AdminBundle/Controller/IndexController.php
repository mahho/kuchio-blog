<?php

namespace Dev\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Dev\AdminBundle\Form\PostType;
use Dev\BlogBundle\Entity\Post;

class IndexController extends Controller
{
    /**
     * @Route("/admin", name="admin_index")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->createQuery("SELECT p FROM DevBlogBundle:Post p ORDER BY p.views DESC")
                    ->setMaxResults(10)->getResult();
        $comments = $em->createQuery("SELECT c FROM DevMainBundle:Comment c WHERE c.visible = 1 ORDER BY c.id DESC")
                    ->setMaxResults(10)->getResult();

        return array(
            'posts' => $posts,
            'comments' => $comments
        );
    }

    /**
     * @Route("/admin/login", name="login")
     * @Template("DevAdminBundle:Security:login.html.twig")
     */
    public function loginAction(Request $request)
    {
    	$session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'DevAdminBundle:Security:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error'         => $error,
            )
        );
    }
}
