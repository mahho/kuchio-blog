<?php

namespace Dev\BlogModulesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class BlogModulesController extends Controller
{
    /**
     * @Route("/blogmodules")
     * @Template()
     */
    public function indexAction()
    {
    	$blogModulesService = $this->get('dev_blog_modules.service');
        return array('modules' => $blogModulesService->renderModules());
    }
}
