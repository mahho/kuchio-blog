<?php
namespace Dev\BlogModulesBundle\Service;

use Doctrine\ORM\EntityManger;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Exception\ParseException;
use Dev\BlogModulesService\Module;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\Container;

class BlogModulesService
{
    private $modules;
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $yaml = new Parser();
        try {
            $this->modules = $yaml->parse(file_get_contents(__DIR__.'/../Resources/config/modules.yml'));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }

    /*public function __construct($em, UrlGeneratorInterface $router)
    {
        $this->em = $em;
        $this->router = $router;
        $yaml = new Parser();
        try {
            $this->modules = $yaml->parse(file_get_contents(__DIR__.'/../Resources/config/modules.yml'));
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }*/

    public function renderModule($moduleName) {
        $moduleName = "Dev\BlogModulesBundle\Module\\".$moduleName.'Module';
        $module = new $moduleName($this->container);
        return $module->render();
    }

    public function renderModules() {
        $result = array();
        foreach ($this->modules as $module) {
            $result[] = $this->renderModule($module);
        }
        return $result;
    }
}
