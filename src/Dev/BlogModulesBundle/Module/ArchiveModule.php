<?php
namespace Dev\BlogModulesBundle\Module;

class ArchiveModule extends AbstractModule 
{
    public function render() {
        return $this->twig->render('Archive.html.twig');
    }
}
