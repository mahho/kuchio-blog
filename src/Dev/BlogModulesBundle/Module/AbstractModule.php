<?php
namespace Dev\BlogModulesBundle\Module;

use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\Extension\RoutingExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\Container;

abstract class AbstractModule implements ModuleInterface
{
    protected $em;
    protected $twig;
    protected $container;

    public function __construct(Container $container) {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->twig = $this->container->get('twig');
        // $this->twig->setLoader(new \Twig_Loader_Filesystem(__DIR__.'/../Resources/modules/'));
        $this->twig->getLoader()->addPath(__DIR__.'/../Resources/modules/');

        // $this->twig = new \Twig_Environment();
        // $this->twig->addExtension(new TemplatingExtension());
        // var_dump($this->twig);
        // exit;

        // $this->twig->addExtension(new RoutingExtension($container->get('router')));
        // $this->twig->addExtension(new FormExtension());

    }
}
