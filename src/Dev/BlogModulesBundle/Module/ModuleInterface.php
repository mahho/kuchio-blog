<?php
namespace Dev\BlogModulesBundle\Module;

use Symfony\Component\DependencyInjection\Container;

interface ModuleInterface
{
    /** 
     * Constructor 
     */
    public function __construct(Container $container);

	/**
	 * Executes module 
	 */
	public function render();


}
