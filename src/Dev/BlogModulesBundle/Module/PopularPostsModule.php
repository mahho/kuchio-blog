<?php
namespace Dev\BlogModulesBundle\Module;

class PopularPostsModule extends AbstractModule 
{
    public function render() {
        $query = "SELECT p FROM DevBlogBundle:Post p WHERE p.visible = 1 ORDER BY p.views DESC, p.addedDate ASC";
        $popularPosts = $this->em->createQuery($query)
                ->setMaxResults(10)->getResult();
        return $this->twig->render('PopularPosts.html.twig', array('popularPosts' => $popularPosts));
    }
}
