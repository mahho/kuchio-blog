<?php
namespace Dev\BlogModulesBundle\Module;

class SubscriptionModule extends AbstractModule 
{
    public function render() {
        return $this->twig->render('Subscription.html.twig');
    }
}
