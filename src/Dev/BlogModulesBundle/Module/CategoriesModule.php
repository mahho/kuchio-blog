<?php
namespace Dev\BlogModulesBundle\Module;

class CategoriesModule extends AbstractModule 
{
    public function render() {
        $categories = $this->em->getRepository("DevMainBundle:Category")->findAll();
        return $this->twig->render('Categories.html.twig', array('categories' => $categories));
    }
}
