<?php
namespace Dev\BlogModulesBundle\Module;

class SearchModule extends AbstractModule 
{
    public function render() {
        $form = $this->container->get('form.factory')
                ->createNamedBuilder('search', 'form')
                ->setAction($this->container->get('router')->generate('dev_search'))
                ->setMethod('POST')
                ->add('phrase', 'text', array('label' => false))
                ->getForm();

        return $this->twig->render('Search.html.twig', array('form' => $form->createView()));
    }
}
