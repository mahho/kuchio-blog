<?php

namespace Dev\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Page
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Dev\CmsBundle\Entity\PageRepository")
 */
class Page
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
     * @var boolean
     *
     * @ORM\Column(name="added_to_menu", type="boolean")
     */
    private $addedToMenu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="added_date", type="datetime")
     */
    private $addedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display_date", type="boolean")
     */
    private $displayDate;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display_title", type="boolean")
     */
    private $displayTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display_content", type="boolean")
     */
    private $displayContent;

    /**
     * @var array
     *
     * @ORM\Column(name="pictures", type="json_array", nullable=true)
     */
    private $pictures;

    /**
     * @var boolean
     *
     * @ORM\Column(name="display_pictures", type="boolean")
     */
    private $displayPictures;

    /**
     * @var \stdClass
     *
     * @ORM\OneToMany(targetEntity="Dev\MainBundle\Entity\Comment", mappedBy="page")
     */
    private $comments;

    /**
     * @ORM\ManyToOne(targetEntity="Dev\MainBundle\Entity\Category", inversedBy="pages")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="display_comments", type="boolean")
     */
    private $displayComments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comments_enabled", type="boolean")
     */
    private $commentsEnabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer", options={"default" = 0})
     */
    private $views;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Page
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set addedToMenu
     *
     * @param boolean $addedToMenu
     * @return Page
     */
    public function setAddedToMenu($addedToMenu)
    {
        $this->addedToMenu = $addedToMenu;

        return $this;
    }

    /**
     * Get addedToMenu
     *
     * @return boolean 
     */
    public function getAddedToMenu()
    {
        return $this->addedToMenu;
    }

    /**
     * Set addedDate
     *
     * @param \DateTime $addedDate
     * @return Page
     */
    public function setAddedDate($addedDate)
    {
        $this->addedDate = $addedDate;

        return $this;
    }

    /**
     * Get addedDate
     *
     * @return \DateTime 
     */
    public function getAddedDate()
    {
        return $this->addedDate;
    }

    /**
     * Set displayDate
     *
     * @param boolean $displayDate
     * @return Page
     */
    public function setDisplayDate($displayDate)
    {
        $this->displayDate = $displayDate;

        return $this;
    }

    /**
     * Get displayDate
     *
     * @return boolean 
     */
    public function getDisplayDate()
    {
        return $this->displayDate;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set displayTitle
     *
     * @param boolean $displayTitle
     * @return Page
     */
    public function setDisplayTitle($displayTitle)
    {
        $this->displayTitle = $displayTitle;

        return $this;
    }

    /**
     * Get displayTitle
     *
     * @return boolean 
     */
    public function getDisplayTitle()
    {
        return $this->displayTitle;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set displayContent
     *
     * @param boolean $displayContent
     * @return Page
     */
    public function setDisplayContent($displayContent)
    {
        $this->displayContent = $displayContent;

        return $this;
    }

    /**
     * Get displayContent
     *
     * @return boolean 
     */
    public function getDisplayContent()
    {
        return $this->displayContent;
    }

    /**
     * Set pictures
     *
     * @param array $pictures
     * @return Page
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;

        return $this;
    }

    /**
     * Get pictures
     *
     * @return array 
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * Set displayPictures
     *
     * @param boolean $displayPictures
     * @return Page
     */
    public function setDisplayPictures($displayPictures)
    {
        $this->displayPictures = $displayPictures;

        return $this;
    }

    /**
     * Get displayPictures
     *
     * @return boolean 
     */
    public function getDisplayPictures()
    {
        return $this->displayPictures;
    }

    /**
     * Set displayComments
     *
     * @param boolean $displayComments
     * @return Page
     */
    public function setDisplayComments($displayComments)
    {
        $this->displayComments = $displayComments;

        return $this;
    }

    /**
     * Get displayComments
     *
     * @return boolean 
     */
    public function getDisplayComments()
    {
        return $this->displayComments;
    }

    /**
     * Set commentsEnabled
     *
     * @param boolean $commentsEnabled
     * @return Page
     */
    public function setCommentsEnabled($commentsEnabled)
    {
        $this->commentsEnabled = $commentsEnabled;

        return $this;
    }

    /**
     * Get commentsEnabled
     *
     * @return boolean 
     */
    public function getCommentsEnabled()
    {
        return $this->commentsEnabled;
    }

    /**
     * Add comments
     *
     * @param \Dev\MainBundle\Entity\Comment $comments
     * @return Page
     */
    public function addComment(\Dev\MainBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Dev\MainBundle\Entity\Comment $comments
     */
    public function removeComment(\Dev\MainBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set category
     *
     * @param \Dev\MainBundle\Entity\Category $category
     * @return Page
     */
    public function setCategory(\Dev\MainBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Dev\MainBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set views
     *
     * @param integer $views
     * @return Page
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * Get views
     *
     * @return integer 
     */
    public function getViews()
    {
        return $this->views;
    }
}
