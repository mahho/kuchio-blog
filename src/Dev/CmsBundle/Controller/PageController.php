<?php

namespace Dev\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Dev\CmsBundle\Entity\Page;
use Dev\CmsBundle\Form\PageType;

/**
 * Page controller.
 *
 * @Route("/page")
 */
class PageController extends Controller
{

    /**
     * Lists all Page entities.
     *
     * @Route("/", name="page")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DevCmsBundle:Page')->findAll();

        return array(
            'entities' => $entities,
        );
    }
  
    /**
     * Finds and displays a Page entity.
     *
     * @Route("/{id}/{slug}", name="page_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DevCmsBundle:Page')->findOneBy(array('id' => $id, 'slug' => $slug));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Page entity.');
        }

        // $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            // 'delete_form' => $deleteForm->createView(),
        );
    }
}
