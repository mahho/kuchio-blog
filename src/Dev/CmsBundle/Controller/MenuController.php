<?php

namespace Dev\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MenuController extends Controller
{
    /**
     * @Route("/cms/menu", name="cms_menu")
     * @Template()
     * Tylko do renderowania w layout
     */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();

    	$pages = $em->createQuery("SELECT p FROM DevCmsBundle:Page p WHERE p.addedToMenu = 1 ORDER BY p.addedDate DESC")
    		->getResult();

    	return $this->render('DevCmsBundle:Menu:index.html.twig', array('pages' => $pages));
        //return array('posts' => $posts);
    }
}
