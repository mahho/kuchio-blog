<?php

namespace Dev\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('visible')
            ->add('addedDate')
            ->add('displayDate')
            ->add('title')
            ->add('displayTitle')
            ->add('content')
            ->add('displayContent')
            ->add('pictures')
            ->add('displayPictures')
            ->add('comments')
            ->add('displayComments')
            ->add('canAddComments')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Dev\BlogBundle\Entity\Post'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'dev_blogbundle_post';
    }
}
