<?php

namespace Dev\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Dev\BlogBundle\Model\BasePost;

/**
 * Post
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Dev\BlogBundle\Entity\PostRepository")
 */
class Post extends BasePost
{

}
