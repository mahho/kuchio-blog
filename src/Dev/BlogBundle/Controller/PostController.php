<?php

namespace Dev\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Dev\BlogBundle\Entity\Post;
use Dev\BlogBundle\Form\PostType;
use Dev\MainBundle\Entity\Comment;
use Dev\MainBundle\Form\CommentType;

/**
 * Post controller.
 *
 * @Route("/post")
 */
class PostController extends Controller
{
    /**
     * Finds and displays a Post entity.
     *
     * @Route("/{id}/{slug}", name="post_show")
     * @Method("GET|POST")
     * @Template()
     */
    public function showAction(Request $request, $id, $slug)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DevBlogBundle:Post')->findOneBy(array('id' => $id, 'slug' => $slug));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Post entity.');
        }

        // $user = $this->get('security.context');
        // var_dump($user);
        // exit;
        // if (false === $user->getRoles()) {
            $increasedViews = $entity->getViews() + 1;
            $entity->setViews($increasedViews);
            $em->persist($entity);
            $em->flush();
        // }

        // $deleteForm = $this->createDeleteForm($id);
        $comment = new Comment();
        $commentForm = $this->createForm(new CommentType(), $comment);

        $commentForm->handleRequest($request);
        if ($commentForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment->setAddedDate(new \DateTime());
            $comment->setVisible(true);
            $comment->setIpAddress($request->getClientIp());
            $comment->setPost($entity);
            $em->persist($comment);
            $em->flush();
            $url = $this->generateUrl('post_show', array('id' => $entity->getId(), 'slug' => $entity->getSlug()));
            return $this->redirect($url."#comments");
        }

        return array(
            'entity'      => $entity,
            'commentForm' => $commentForm->createView()
            // 'delete_form' => $deleteForm->createView(),
        );
    }

}
